import React, { Component } from 'react';
import { connect } from 'react-redux';
import './App.css';
import { changeFirstName as _changeFirstName } from './actions/userActions';
import { changeLastName as _changeLastName } from './actions/userActions';
import alertButton from './components/alertButton';


class App extends Component {

  changeFirstName = (e) => {
    this.props.changeFirstName(e.target.value);
  };

  changeLastName = (e) => {
    this.props.changeLastName(e.target.value);
  };

  render() {
    return (
      <div className="App">
        <input value={this.props.user.firstName} onChange={this.changeFirstName} />
        <input value={this.props.user.lastName} onChange={this.changeLasttName} />

        <pre style={{textAlign: "left"}}>
         {
           JSON.stringify(this.props, null, 2)
         }
        </pre>
<alertButton> 
  "Hello my Name is "
</alertButton>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  changeFirstName: (value) => dispatch(_changeFirstName(value)),
  changeLastName: (value) => dispatch(_changeLastName(value))
});


export default connect(mapStateToProps, mapDispatchToProps)(App);
